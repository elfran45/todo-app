const express = require('express');
const path = require('path');
const bodyParser= require('body-parser');
const cors = require('cors')

const ListsRouter = require('./routes/ListsRoutes');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'client/build')));

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }))

app.get("/", (req, res) => {
  res.send('MERN stack setup, use MVC architecture')
});

app.use('/api/v1/lists', ListsRouter);

module.exports = app;
