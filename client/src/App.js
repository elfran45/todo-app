import React from 'react';
import {Component} from 'react';
import './App.css';

import ListsContainer from './components/ListsContainer';


class App extends Component {

  constructor(){
    super();
    this.state = {
      isLoaded: false,
      data: null
    }
  }

  render() {
    
    return (
      <div className="App-wrapper">
          <ListsContainer/>
      </div>
    );
  }
  
}

export default App;
