import React from 'react';
import './Submit.css';

function Submit(props) {
    return (
        <div className="submit">
           <form >
           <input maxLength="58" className="submit-input" type="text" name="" 
           
           placeholder="Enter new Todo ..."
           value={props.value}
           onChange={props.handleChangeValue}
           />
           <button className="todo-list-btn" type="submit" onClick={props.handleSubmit}>Create</button>
           </form>
        </div>
    )
}

export default Submit;