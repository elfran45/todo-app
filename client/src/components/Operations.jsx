import React from 'react';
import {Delete, Create} from '@material-ui/icons';


function Operations(props){
    return (
        <div className="operations">
            <button className="btn" onClick={() => {props.handleUpdate(props.id)}}>
                <Create fontSize="small" />
            </button>
            <button className="btn delete-btn" onClick={() => {props.handleDelete(props.id)}}>
                <Delete fontSize="small" />
            </button>
        </div>
    )
}

export default Operations;
 