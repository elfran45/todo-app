import React from 'react';
import {Component} from 'react';
import '../components/List.css';
import axios from 'axios';

import TodosWrapper from '../components/TodosWrapper';
import Submit from '../components/Submit';

class List extends Component {

  constructor(props){
    super(props);
    
    this.state = {
      name: "",
      todos: [],
      textInput:"",
      updateInput:"",
      toggleDisplay: false,
      
    }
  }

    handleChangeValue = e => {
        e.preventDefault()
        this.setState({textInput: e.target.value})
    }
    
    handleChangeUpdateValue = e => {
        e.preventDefault()
        this.setState({updateInput: e.target.value})
    }

    componentWillReceiveProps(newProps) {
        this.setState({listId: newProps.listId}, () => {
            this.getList()
        });
        
    }

// Axios requests
    getList(){
        axios.get(`/api/v1/lists/${this.props.listId}`)
        .then(res => {
            this.setState({
                name: res.data.data.name,
                todos: res.data.data.todos})
        })
        .catch(err => err)
    }

    postTodo(newTodo){
        axios.post(`/api/v1/lists/${this.props.listId}`, newTodo)
        .then(res => {
            this.getList()
            this.setState({textInput: ""})
        })
    }

    deleteTodo(todoId){
        const todos= this.state.todos
        const selectedTodo = this.state.todos[todoId]._id
        axios.delete(`/api/v1/lists/${this.props.listId}/${selectedTodo}`)
        .then(res => {
            this.getList()
            this.setState({todos: []});
            todos.splice(todoId, 1);
            this.setState({todos: todos})

        })
    }

    updateTodo(todoId){
        if(this.state.updateInput === "") return;
        const selectedTodo = this.state.todos[todoId]._id
        const currentCheck = this.state.todos[todoId].check;
        axios.patch(`/api/v1/lists/${this.props.listId}/${selectedTodo}`, {check: currentCheck, todoText: this.state.updateInput} )
        .then(res => {
            this.getList();
        })
    }

    
    // Handlers
    handleSubmit = event => {
        event.preventDefault()
        if(this.state.textInput === "") return;
        const newTodo = {
            todoText: this.state.textInput,
        }
        this.postTodo(newTodo)
    }

    handleDeleteTodo = (todoId) => {
        this.deleteTodo(todoId);
        
    }

    handleUpdateTodo = (todoId) => {
        this.updateTodo(todoId)
    }

    checkUpdateTodo = (todoId) => {
        const selectedTodo = this.state.todos[todoId]._id;
        const currentCheck = this.state.todos[todoId].check;
        const currentTodoText = this.state.todos[todoId].todoText;
        axios.patch(`/api/v1/lists/${this.props.listId}/${selectedTodo}`, {check: !currentCheck, todoText: currentTodoText} )
        .then(res => {
        })
    }

    componentDidMount() {
        this.getList();
    }

    render() {
    return (
      <div className={this.props.slideClass} >
        <TodosWrapper 
            todos={this.state.todos} 
            handleChangeValue={this.handleChangeValue}
            handleChangeUpdateValue={this.handleChangeUpdateValue} 
            handleDeleteTodo={this.handleDeleteTodo}
            handleUpdateTodo={this.handleUpdateTodo}
            checkUpdateTodo={this.checkUpdateTodo}
             />
        <Submit className="todo-submit"
            handleChangeValue={this.handleChangeValue} 
            handleSubmit={this.handleSubmit} 
            value={this.state.textInput}/>
      </div>
    );
  }
}

export default List;
