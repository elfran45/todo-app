import React from 'react';
import './TodosWrapper.css';
import Todo from './Todo'

function TodosWrapper(props) {

    return (
        <div className="todos-wrapper">
            {props.todos.map((todo, index) => {
                return (<Todo 
                    key={index} 
                    id={index} 
                    check={todo.check}
                    checkUpdateTodo={props.checkUpdateTodo} 
                    date={todo.date} 
                    text={todo.todoText} 
                    handleDeleteTodo={props.handleDeleteTodo}
                    handleUpdateTodo={props.handleUpdateTodo}
                    handleChangeUpdateValue={props.handleChangeUpdateValue}
                    />)
                }
            )}
        </div>
    )
}

export default TodosWrapper;