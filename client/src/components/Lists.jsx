import React from 'react';
import {Component} from 'react';
import './Lists.css';
import List from './List';
import Operations from './Operations';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

class Lists extends Component {
    constructor(props){
        super(props)
        this.state = {
            toggleList: false,
            slideClass: "slideup"
        }
    }
    
    toggleList = () => {
        this.setState({toggleList: !this.state.toggleList}, () => {
            this.state.toggleList ? this.setState({slideClass: "slidedown"})
                : this.setState({slideClass: "slideup"})
        });
    }

    

    render(){
        return (
        <div className="lists">
            <div className="list-name">
                <div className="list-name name-wrap">
                <button className="toggle-btn" onClick={this.toggleList}>
                    <KeyboardArrowDownIcon fontSize="small" />
                </button>
                <input maxLength="24" defaultValue={this.props.listName} onChange={this.props.handleUpdateValue} onFocus={e => e.target.select()} />
                </div>
                <Operations id={this.props.id} handleUpdate={this.props.handleUpdateList} handleDelete={this.props.handleDeleteList}/>
            </div>
            
            <div >
                <List slideClass={this.state.slideClass} listId={this.props.listId} />
            </div>
            
        </div>

        )
    }
}

export default Lists;