import React from 'react';
import {Component} from 'react';
import './Todo.css'
import Operations from './Operations';

class Todo extends Component{
    constructor(props){
        super(props)
        this.state = {
            check: false,
            checkColor: '',
            color: '',
            textDecoration: '',
            
        }
        
    }

    changeCheckStyle(){
        if(this.state.check === true){
            this.setState({checkColor: '#4061B6', color: '#00000087', textDecoration: 'line-through'})
        } else {
            this.setState({checkColor: '', color: 'black', textDecoration: ''});
        }
    }
    
    check(){
        this.setState({check: !this.state.check}, () => {
            this.changeCheckStyle();
        })
    }

    loadCheck(){
        this.setState({check: this.props.check}, () => {
            this.changeCheckStyle()
        })
    }

    componentWillReceiveProps(newProps) {
        this.setState({check: newProps.check}, () => {
            this.loadCheck()
        });
        
    }

    componentDidMount(){
        this.loadCheck();
    }
    
    render(){
        
        const {checkColor, textDecoration, backgroundColor, color} = this.state;
        return (
            <div className="todo-wrapper" style={{backgroundColor: backgroundColor}}>
                <div className="check" onClick={() => {
                    this.props.checkUpdateTodo(this.props.id);
                    this.check();
                    }
                    }>
                    <div className="outter-check">
                        <div className="inner-check" style={{backgroundColor: checkColor}}></div>
                    </div>
                </div>
                <div className="todo-text" >
                    <div className="date">{this.props.date}</div>
                    
                    <textarea
                        maxLength="58"
                        className="update-input" 
                        type="text" defaultValue={this.props.text} 
                        style={{textDecoration: textDecoration, color: color}}
                        onChange={this.props.handleChangeUpdateValue}
                        onFocus={e => e.target.select()} 
                    />
                </div>

                <Operations id={this.props.id} handleUpdate={this.props.handleUpdateTodo} handleDelete={this.props.handleDeleteTodo}/>
            
            </div>
        )
    }
}

export default Todo;