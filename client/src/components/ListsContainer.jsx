import React from 'react';
import {Component} from 'react';
import axios from 'axios';
import './ListsContainer.css';

import Lists from './Lists';


class ListsContainer extends Component {

  constructor(){
    super();
    this.state = {
      isLoaded: false,
      data: null,
      textInput: "",
      updateInput: ""
    }
  }

    handleChangeValue = e => {
        e.preventDefault()
        this.setState({textInput: e.target.value})
}

    handleChangeUpdateValue = e => {
        e.preventDefault()
        this.setState({updateInput: e.target.value})
}

  getLists(){
    axios.get('/api/v1/lists')
    .then(res => {
      this.setState({...this.state, isLoaded: true, data: {lists: res.data.data}})
    })
  }

  componentDidMount(){
    this.getLists()
  }

 

    postList(newList){
        axios.post(`/api/v1/lists`, newList)
        .then(res => {
            this.setState({textInput: ""});
            this.getLists()
          })
    }

    deleteList = (listId) => {
        const lists = this.state.data.lists
        const selectedList = this.state.data.lists[listId]._id
        axios.delete(`/api/v1/lists/${selectedList}`)
        .then(res => {
          this.getLists()
        this.setState({ data: {lists: []}});
        lists.splice(listId, 1)
        this.setState({ data: {lists: lists}})
        })
    }

    updateList(listId){
        if(this.state.updateInput === "") return;
        const selectedList = this.state.data.lists[listId]._id
        axios.patch(`/api/v1/lists/${selectedList}`, {name: this.state.updateInput} )
        .then(res => {
          this.getLists()
        })
    }

    // Handlers
    handleSubmit = event => {
        event.preventDefault()
        if(this.state.textInput === "") return;
        const newList = {
            name: this.state.textInput,
            todos: []
        }
        this.postList(newList)
    }

    handleDeleteList = (listId) => {
        this.deleteList(listId);
        
    }

    handleUpdateList = (listId) => {
        this.updateList(listId)
    }

  

  render() {
    const {isLoaded, data} = this.state;
    
    return (

      <div className="App">
          <div className="header">
            <div className="name">NoteBog</div>
            <div className="list-submit">
              <form >
              <input className="submit-input" type="text" name="" 
              maxLength="24"
              placeholder="Enter new List ..."
              value={this.state.textInput}
              onChange={this.handleChangeValue}
              />
              <button className="list-submit-btn" type="submit" onClick={this.handleSubmit}>Create</button>
              </form>
            </div>
            
        </div>

        <div className="wrapper">
          {
            isLoaded?
            data.lists.map((list, index) => {
            return <Lists 
            key={index} 
            id={index} 
            listId={list._id} 
            listName={list.name}
            handleUpdateValue={this.handleChangeUpdateValue} 
            handleUpdateList={this.handleUpdateList} 
            handleDeleteList={this.handleDeleteList}/>
            })  
            : console.log('nothing')
          }
          </div>
          
        
      </div>
    );
  }
  
}

export default ListsContainer;
