const List = require('../models/listModel');
const { ObjectId } = require('mongodb');



exports.getTodoText = async (req, res) => {
    const todo = await List.findOneAndUpdate({"todos._id" : req.params.todoid},{$set:{"todos.$.todoText": req.body}})
    res.status(200).json({
        status: 'success',
        data: todo
    })
}

exports.newTodo = async (req, res) => {
    const newTodo = await List.update(
        {'_id': req.params.id},
        {$push: {"todos": req.body}})
    res.status(200).json({
        status: 'success',
        message: `Todo created`,
        data: newTodo
        
    })
};

exports.updateTodo = async (req, res) => {
    const todo = await List.findOneAndUpdate({"todos._id" : req.params.todoid},{$set:
        {"todos.$.todoText": req.body.todoText,
         "todos.$.check": req.body.check}})
    res.status(200).json({
        status: 'success',
        data: todo
    })
}

exports.deleteTodo = async (req, res) => {
    const todo = await List.update(
        {'_id': ObjectId(req.params.id)},
        {$pull: {"todos": {'_id': req.params.todoid}}})
    res.status(200).json({
        status: 'success',
        message: `${todo} has been deleted`,
        data: todo
    })
}


