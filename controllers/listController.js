const List = require('../models/listModel');

exports.newList = async (req, res) => {
    const newList = await List.create(req.body);
    res.status(200).json({
        status: 'success',
        data: newList
        
    })
};

exports.findLists = async (req, res) => {
    const lists = await List.find({}, {_id: 1, name:1});
    res.status(200).json({
        status: 'success',
        data: lists
    })
};

exports.findList = async (req, res) => {
    const list = await List.findOne({_id: req.params.id});
    res.status(200).json({
        status: 'success',
        data: list
    })
}

exports.updateList = async (req, res) => {
    console.log(req.params.id);
    const list = await List.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({
        status: 'success',
        data: list
    })
}

exports.deleteList = async (req, res) => {
    console.log(req.params.id);
    const list = await List.findByIdAndDelete(req.params.id)
    res.status(200).json({
        status: 'success',
        message: `${list.name} profile has been deleted`,
        data: list
    })
}