# NoteBog - todoApp  
https://notebog.herokuapp.com/

## Description  
  
A todo app with multiple lists. Being a dynamic web app I used frontend React
Built with M.E.R.N stack  



## Technologies Used  
  
MongoDB 
* Mongoose

Node  
* Express 

React  
* Axios
* Material UI

Deployment  
* MongoDB Atlas  
* Heroku  
  
![notebog](/uploads/148874ea20f3ec4e20df2ac1c3ce5139/notebog.png)



