const mongoose = require('mongoose');

const date = new Date();

const listSchema = new mongoose.Schema({
    name: String,
    todos: [
        {
            date: {type: String, default: `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`},
            todoText: String,
            check: {type: Boolean, default: false}
        }
    ]
})

const List = mongoose.model('List', listSchema);

module.exports = List;