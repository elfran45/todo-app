const mongoose = require('mongoose');
const dotenv = require('dotenv')
const path = require('path')
require('dotenv').config();
dotenv.config({ path: './config.env' });
const app = require('./app');
const express = require('express');

mongoose
  .connect( process.env.DATABASE, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true 
  })
  .then(() => console.log('Connection to DB successful'));

  
  // Este codigo es para servir assets en production
  if(process.env.NODE_ENV === 'production') {
    app.use(express.static('client/build'))

    app.get('*', (req, res) => {
      res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    })
  }

app.listen(process.env.PORT || 3002, () => {
  console.log(`App running on port ${process.env.PORT} or 3002...`);
});
