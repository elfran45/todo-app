const express = require('express');
const ListController = require('../controllers/listController');
const TodoController = require('../controllers/todoController');

const router = express.Router();

router
    .route('/')
    .get(ListController.findLists)
    .post(ListController.newList)


router
    .route('/:id')
    .get(ListController.findList)
    .patch(ListController.updateList)
    .delete(ListController.deleteList)

router
    .route('/:id/')
    .post(TodoController.newTodo)

router
    .route('/:id/:todoid')
    .get(TodoController.getTodoText)
    .patch(TodoController.updateTodo)
    .delete(TodoController.deleteTodo)

module.exports = router;